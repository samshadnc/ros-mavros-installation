#!/usr/bin/env python
import rospy
from geometry_msgs.msg import PoseStamped

def publish_waypoint():
    rospy.init_node('setpoint_position_publisher', anonymous=True)
    waypoint_pub = rospy.Publisher('/sitl/mavros/setpoint_position/local', PoseStamped, queue_size=10)
    rate = rospy.Rate(10)  # 10 Hz

    # Wait for the connection to establish
    while not rospy.is_shutdown() and waypoint_pub.get_num_connections() == 0:
        rate.sleep()

    waypoint = PoseStamped()
    waypoint.header.stamp = rospy.Time.now()
    waypoint.header.frame_id = "local_origin"  # Use the appropriate frame ID
    waypoint.pose.position.x = 10.0
    waypoint.pose.position.y = 10.0
    waypoint.pose.position.z = 10.0
    # Orientation (assuming quaternion x, y, z, w - may need to be adjusted)
    waypoint.pose.orientation.x = 0.0
    waypoint.pose.orientation.y = 0.0
    waypoint.pose.orientation.z = 0.0
    waypoint.pose.orientation.w = 1.0

    try:
        while not rospy.is_shutdown():
            waypoint.header.stamp = rospy.Time.now()
            waypoint_pub.publish(waypoint)
            rate.sleep()
    except rospy.ROSInterruptException:
        pass

if __name__ == '__main__':
    try:
        publish_waypoint()
    except rospy.ROSInterruptException:
        pass
