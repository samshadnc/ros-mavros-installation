import rospy
from mavros_msgs.msg import PositionTarget
from std_msgs.msg import Header
import time

# Function to move the drone to a specific coordinate
def move_drone(local_position_publisher, x, y, z, yaw):
    position_target = PositionTarget()
    position_target.header = Header()
    position_target.header.stamp = rospy.Time.now()
    position_target.coordinate_frame = PositionTarget.FRAME_LOCAL_NED
    position_target.type_mask = PositionTarget.IGNORE_VX | PositionTarget.IGNORE_VY | PositionTarget.IGNORE_VZ | PositionTarget.IGNORE_AFX | PositionTarget.IGNORE_AFY | PositionTarget.IGNORE_AFZ | PositionTarget.FORCE | PositionTarget.IGNORE_YAW_RATE
    position_target.position.x = x
    position_target.position.y = y
    position_target.position.z = z
    position_target.yaw = yaw
    local_position_publisher.publish(position_target)

# Main sequence
if __name__ == '__main__':
    rospy.init_node('simple_waypoint_script')

    # Create a publisher for sending position targets
    local_position_publisher = rospy.Publisher('/sitl/mavros/setpoint_raw/local', PositionTarget, queue_size=10)

    # Define waypoints
    waypoints = [(10, 0, 10), (10, 10, 10), (0, 10, 10), (0, 0, 10)]

    try:
        rate = rospy.Rate(10)  # Set a rate (Hz) to send waypoints
        time.sleep(5)  # Wait for a few seconds to ensure ROS is ready

        for point in waypoints:
            move_drone(local_position_publisher, *point, 0)
            rospy.sleep(10)  # 10-second delay at each waypoint
            rate.sleep()

    except rospy.ROSInterruptException:
        pass
