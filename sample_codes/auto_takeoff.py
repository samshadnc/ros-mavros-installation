import rospy
import time
from mavros_msgs.msg import PositionTarget
from mavros_msgs.srv import CommandBool, SetMode, CommandTOL
from std_msgs.msg import Header

# Initialize the ROS node
rospy.init_node('drone_control_script')

# Create a publisher for sending position targets
local_position_publisher = rospy.Publisher('/sitl/mavros/setpoint_raw/local', PositionTarget, queue_size=10)

# Create service proxies for arming, changing the flight mode, and takeoff
arming_client = rospy.ServiceProxy('/sitl/mavros/cmd/arming', CommandBool)
set_mode_client = rospy.ServiceProxy('/sitl/mavros/set_mode', SetMode)
takeoff_client = rospy.ServiceProxy('/sitl/mavros/cmd/takeoff', CommandTOL)

# Function to change the flight mode
def set_flight_mode(mode):
    rospy.wait_for_service('/sitl/mavros/set_mode')
    try:
        mode_response = set_mode_client(custom_mode=mode)
        return mode_response.mode_sent
    except rospy.ServiceException as e:
        print("Service call failed: %s" % e)

# Function to arm the drone
def arm_drone():
    rospy.wait_for_service('/sitl/mavros/cmd/arming')
    try:
        arm_response = arming_client(True)
        return arm_response.success
    except rospy.ServiceException as e:
        print("Service call failed: %s" % e)

# Function to take off
def takeoff_drone(altitude):
    rospy.wait_for_service('/sitl/mavros/cmd/takeoff')
    try:
        takeoff_response = takeoff_client(altitude=altitude)
        return takeoff_response.success
    except rospy.ServiceException as e:
        print("Service call failed: %s" % e)

# Function to move the drone to a specific coordinate
def move_drone(x, y, z, yaw):
    position_target = PositionTarget()
    position_target.header = Header()
    position_target.header.stamp = rospy.Time.now()
    position_target.coordinate_frame = PositionTarget.FRAME_LOCAL_NED
    position_target.type_mask = PositionTarget.IGNORE_VX | PositionTarget.IGNORE_VY | PositionTarget.IGNORE_VZ | PositionTarget.IGNORE_AFX | PositionTarget.IGNORE_AFY | PositionTarget.IGNORE_AFZ | PositionTarget.FORCE | PositionTarget.IGNORE_YAW_RATE
    position_target.position.x = x
    position_target.position.y = y
    position_target.position.z = z
    position_target.yaw = yaw
    local_position_publisher.publish(position_target)

# Main sequence
if __name__ == '__main__':
    try:
        # Set to GUIDED mode
        set_flight_mode('GUIDED')
        
        # Arm the drone
        arm_drone()

        # Takeoff to 10 meters altitude
        takeoff_drone(10)

        # Delay to stabilize after takeoff
        rospy.sleep(10)

        # Navigate through waypoints with a delay between each
        waypoints = [(10, 0, 10), (10, 10, 10), (0, 10, 10), (0, 0, 10)]
        for point in waypoints:
            move_drone(*point, 0)
            rospy.sleep(10)  # 10-second delay between waypoints

        rospy.spin()

    except rospy.ROSInterruptException:
        pass
