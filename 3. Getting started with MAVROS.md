# Try only after succefully installing Ardupilot, ROS and MAVROS 

***Pre requesits:***

Befor working with mavros it is required to download and and keep the [apmsitl.launch](mavros_files/apmsitl.launch) file to the location :~/catkin_ws/src/mavros/mavros/launch
also replace the file [node.launch](mavros_files/node.launch) to ~/catkin_ws/src/mavros/mavros/launch

# Launching MAVROS 

## 1. Start ArduPilot SITL(Terminal 1)

Open a new terminal and start the ArduPilot SITL. Keep it running throughout the experiment.
```
sim_vehicle.py -v ArduCopter --map --console

```

## 2. Start Rosmaster(Terminal 2)

Before launching any ROS nodes, initialize Rosmaster. Keep Rosmaster running throughout the experiment.

```
roscore
```


## 3. Launch Mavros node(Terminal 3)

To launch the MAVROS package, use the standard Roslaunch command:

roslaunch <package_name> <launchfile>

for mavros 
```
roslaunch mavros apmsitl.launch
```
![mavros](images/mavros.png)

If the results are like above you can assume the mavros package is started 
Once launched, verify the status of the MAVROS package using the following commands:

```
rosnode list  # List all active nodes
rostopic list  # List all ROS topics
rosservice list  # List all services

```

## Customising Mavros

You can always customise the parameters inside the apmsitl.launch file

![apmsitl.launch](images/sitl.png)

Line 5: Define the Flight Controller Unit (FCU) connection URL. Replace it with the hardware port address for the flight controller connection.

Line 6: Define the Ground Control Station (GCS) bridge IP for sharing MAVLink packets to a remote PC.

Line 7: Define the MAVLink system ID (default is 1).

Line 12: Define the node name. You can replace "sitl" with your custom name.

# Controlling the Drone Using MAVROS 
MAVROS provides a variety of control topics and services to send control guidance and navigational commands to the flight controller or SITL (Software in the Loop).

#### Note: Not all listed topics and services may be compatible with your ArduPilot SITL or flight controller. It is essential to verify that the topic you intend to use works with your setup.

This guide lists several topics and services to help you get started with MAVROS.

## Mode Change and Takeoff


MAVROS offers specific ROS services for changing modes, arming the drone, and taking off:

- /sitl/mavros/set_mode   
- /sitl/mavros/cmd/arming
- /sitl/mavros/cmd/takeoff

You can invoke these services using the rosservice call option in the terminal or within a script. Here is a sample script for taking off:
[takeoff_script](mavros_files/auto_takeoff.py)

Quick Tip: To view the options of a ROS service or topic, use the double-tab feature in the terminal:

rosservice call <service_name> <double_tab>
then edit the fields inside the message

```
rosservice call /sitl/mavros/set_mode   "base_mode: 0
custom_mode: 'guided'"

rosservice call /sitl/mavros/cmd/arming "value: true"

rosservice call /sitl/mavros/cmd/takeoff "{min_pitch: 0.0, yaw: 0.0, latitude: 0.0, longitude: 0.0, altitude: 10.0}"
```

## Waypoint Navigation After Takeoff

After the drone has taken off, you can use setpoint topics to send waypoints to the flight controller. MAVROS offers several setpoint topics for this purpose:

- Commonly used topics include /mavros/setpoint_position/local, /mavros/setpoint_position/global, /mavros/setpoint_raw/local, and /mavros/setpoint_raw/global

You can publish control input directly to these topics using the rostopic pub command or write scripts using the rospy library to automate the process:
[waypoint_script](mavros_files/single_waypoint.py), and moving to multiple points
[square](mavros_files/waypoint.py)
#### Note: Before publishing to any control topic, it is crucial to understand the message type and the various fields within the message format, especially the coordinate frame and type_mask field.

Below is an example for publishing to setpoint topics. Ensure the drone is airborne and in guided mode before running these scripts.

rostopic pub <topic_name> <double_tab_for_auto_loading_msg_type> <double_tab_for_expanding_the_message_fields >

```
rostopic pub /sitl/mavros/setpoint_position/local geometry_msgs/PoseStamped  # double tab to expand the msg fields

```

